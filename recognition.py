import argparse
from timeit import default_timer as timer

import cv2
import imageio
import face_recognition
import numpy as np
from py4j.protocol import Py4JJavaError
from pyspark.sql import SparkSession
from pyspark.sql.functions import udf
from pyspark.sql.types import StringType


def compare_faces(face_features):
    def map_distances(value):
        return np.linalg.norm([face_features] - np.array(value), axis=1)[0].item()

    return map_distances


try:
    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument('-f', '--file', help='path to video file to process')
    args = argument_parser.parse_args()

    spark_session = SparkSession \
        .builder \
        .appName("myApp") \
        .config('spark.jars.packages', 'org.mongodb.spark:mongo-spark-connector_2.11:2.3.1')\
        .config("spark.mongodb.input.uri", "mongodb://127.0.0.1:27017/face_recognition.face_features") \
        .config("spark.mongodb.output.uri", "mongodb://127.0.0.1:27017/face_recognition.face_features") \
        .getOrCreate()

    df = spark_session.read.format("com.mongodb.spark.sql.DefaultSource").load()

    filename = args.file
    try:
        if filename:
            video_capture = imageio.get_reader(filename)
        else:
            video_capture = imageio.get_reader('<video0>')
    except RuntimeError as ex:
        argument_parser.exit(-1, ex)

    face_locations = []
    face_names = []
    counter = 0

    for i, frame in enumerate(video_capture):
        # opencv uses BGR by default
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        small_frame = cv2.resize(frame, (0, 0), fx=0.4, fy=0.4)
        # small_frame = original_frame
        if counter == 24:
            counter = 0

        # Only process every other frame of video to save time
        if counter == 0:
            face_names = []
            # Find all the faces and face encodings in the current frame of video
            face_locations = face_recognition.face_locations(small_frame)
            face_encodings = face_recognition.face_encodings(small_frame, face_locations)

            for face_encoding in face_encodings:
                start = timer()
                udf_map_distances = udf(compare_faces(face_encoding), StringType())
                current_face_df = df.withColumn('distance', udf_map_distances('features'))
                selectedRow = current_face_df.orderBy('distance').filter('distance < 0.7').first()
                end = timer()
                print("took {0}".format(end - start))

                if selectedRow is None:
                    name = 'Unknown'
                else:
                    name = selectedRow['name']

                face_names.append(name)
        counter += 1

        print(face_locations)
        print(face_names)
        # Display the results
        for (top, right, bottom, left), name in zip(face_locations, face_names):
            # Scale back up face locations since the frame we detected in was scaled to 1/4 size
            top = int(top / 0.4)
            right = int(right / 0.4)
            bottom = int(bottom / 0.4)
            left = int(left / 0.4)

            # Draw a box around the face
            cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

            # Draw a label with a name below the face
            cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
            font = cv2.FONT_HERSHEY_DUPLEX
            cv2.putText(frame, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)

        # Display the resulting image
        cv2.imshow('Video', frame)

        # Hit 'q' on the keyboard to quit!
        if cv2.waitKey(25) & 0xFF == ord('q'):
            break

    cv2.destroyAllWindows()

except Py4JJavaError as e:
    print(e)
