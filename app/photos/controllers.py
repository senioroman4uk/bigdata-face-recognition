from flask import Blueprint, current_app, send_from_directory

import os

mod_photos = Blueprint('photos', __name__)


@mod_photos.route('/photos/<path:path>')
def index(path):
    photos_folder = os.path.join(current_app.instance_path, 'photos')
    return send_from_directory(photos_folder, path)
