import os

from flask import Blueprint, request, render_template, flash, redirect, url_for, jsonify

from app.decorators import ajax_required

from .helpers import save_uploaded_file, get_path_to_photo
from .face_detector import FaceDetectionService
from .forms import ImageForm, FacesForm
from .models import FaceFeatures

mod_face_features = Blueprint('face_features', __name__)
detector = FaceDetectionService()


@mod_face_features.route('/', methods=['GET', 'POST'])
def load_image():
    form = ImageForm()

    if request.method == 'POST' and form.validate_on_submit():
        path_to_saved_file = save_uploaded_file(form.image.data)
        face_locations = detector.get_face_locations_from_file(path_to_saved_file)
        if len(face_locations) > 0:
            return render_template('face_features/select-faces.html', face_locations=face_locations,
                                   image=os.path.basename(path_to_saved_file))
        else:
            flash('No faces found on your image', 'warning')

    return render_template('face_features/add-face.html', form=form)


@mod_face_features.route('/add_faces', methods=['POST'])
@ajax_required
def add_faces():
    form = FacesForm.from_json(request.json)
    if form.validate_on_submit():
        data = form.data
        face_features = detector.get_face_features(get_path_to_photo(data['filename']), data['locations'])

        entities = []
        for (features, name) in zip(face_features, data['names']):
            entities.append(FaceFeatures(name=name, features=features.tolist()))
        FaceFeatures.objects.insert(entities)

        return jsonify({'message': 'faces added successfully', 'status': 'success'}), 201

    return jsonify(form.errors), 400
