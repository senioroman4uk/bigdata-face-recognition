from mongoengine import *


class FaceFeatures(Document):
    name = StringField(min_length=3)
    features = ListField(FloatField())
