from flask_wtf import FlaskForm
from flask_wtf.file import FileRequired, FileField, FileAllowed
from wtforms import FieldList, FormField, StringField, FloatField
from wtforms.validators import Length, DataRequired


class ImageForm(FlaskForm):
    image = FileField('Image', validators=[FileRequired(), FileAllowed(['gif', 'jpg', 'jpeg', 'png'],
                      'Only "gif", "jpg", "jpeg" and "png" files are supported')])


class FacesForm(FlaskForm):
    # TODO: due to issue with wtfforms form that contains nested forms cannot be serialised properly
    locations = FieldList(FloatField())
    names = FieldList(StringField(validators=[Length(3)]), validators=[DataRequired(), Length(1)])
    filename = StringField(validators=[DataRequired()])

    def validate(self):
        if not super().validate():
            return False

        if len(self.names.data) * 4 != len(self.locations.data):
            self.locations.errors.append('Each face should have four locations')
            return False

        return True
