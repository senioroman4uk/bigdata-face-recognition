import wtforms_json
from flask import Flask
from flask_mongoengine import MongoEngine
from flask_wtf import CSRFProtect

from app.face_features.controllers import mod_face_features
from app.photos.controllers import mod_photos

BLUEPRINTS = [
    mod_face_features,
    mod_photos
]

db = MongoEngine()


def create_app(config=None, app_name=None):
    # if app_name is None:
    #     app_name = Config.DefaultConfig.PROJECT

    app = Flask(app_name)
    __configure_app(app, config)
    __configure_blueprints(app)
    __configure_extensions(app)

    return app


def __configure_app(app, config):
    app.config.from_pyfile('config.py')

    # MODE = os.getenv('APPLICATION_MODE', 'LOCAL')
    # print("Running in %s mode" % MODE)

    # app.config.from_envvar('YOURAPPLICATION_SETTINGS', True)

    if config:
        app.config.from_object(config)


def __configure_blueprints(app):
    for blueprint in BLUEPRINTS:
        app.register_blueprint(blueprint)


def __configure_extensions(app):
    # csrf
    csrf = CSRFProtect()
    csrf.init_app(app)

    db.init_app(app)

    # json forms
    wtforms_json.init()
