from functools import wraps
from flask import request


def ajax_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not request.is_xhr:
            return None, 401

        return f(*args, **kwargs)

    return decorated_function
