import os

from flask import current_app
from werkzeug.utils import secure_filename


def get_path_to_photo(filename: str) -> str:
    filename = secure_filename(filename)
    path = os.path.join(
        current_app.instance_path, 'photos', filename)
    return path


def save_uploaded_file(f):
    path = get_path_to_photo(f.filename)
    f.save(path)
    return path
