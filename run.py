import argparse
import os

from app import create_app

app = create_app(app_name=__name__)

if __name__ == '__main__':
    photos_directory = 'instance/photos'
    if not os.path.exists(photos_directory):
        os.makedirs(photos_directory)

    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--port', default=int(os.environ.get('PORT', 5001)), type=int, help='Specify port for web app to listen too')
    parser.add_argument('--host', default=os.environ.get('HOST', '0.0.0.0'), help='Host for web app to listen too')

    args = parser.parse_args()
    port = args.port
    host = args.host

    app.run(host=host, port=port)
