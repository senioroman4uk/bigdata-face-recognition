from typing import List, Tuple

import face_recognition
import numpy as np


class FaceDetectionService:
    def get_face_locations_from_file(self, path_to_file: str) -> List[Tuple[int, int, int, int]]:
        frame = face_recognition.load_image_file(path_to_file)
        # small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)
        face_locations = face_recognition.face_locations(frame)
        # corrected_face_locations = ((top*4, right*4, bottom*4, left*4) for (top, right, bottom, left) in face_locations)
        # face_encodings = face_recognition.face_encodings(small_frame, face_locations)
        return face_locations

    def get_face_features(self, path_to_file: str, face_locations: List[int]) -> List[np.ndarray]:
        frame = face_recognition.load_image_file(path_to_file)
        face_locations = self.__map_locations_to_tuples(face_locations)
        face_encodings = face_recognition.face_encodings(frame, face_locations)
        return face_encodings

    @staticmethod
    def __map_locations_to_tuples(face_locations: List[int]):
        result = []
        for i in range(0, len(face_locations) // 4, 1):
            result.append(tuple(map(lambda x: int(x), face_locations[i*4:(i+1)*4])))

        return result
